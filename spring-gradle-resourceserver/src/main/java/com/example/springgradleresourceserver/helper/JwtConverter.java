package com.example.springgradleresourceserver.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.JwtAccessTokenConverterConfigurer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

@Component
public class JwtConverter extends DefaultAccessTokenConverter implements JwtAccessTokenConverterConfigurer {

	@Override
	public void configure(JwtAccessTokenConverter converter) {
		// TODO Auto-generated method stub
		converter.setAccessTokenConverter(this);
	}

	@Override
	public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
		// TODO Auto-generated method stub
//		return super.extractAuthentication(map);
		OAuth2Authentication auth = super.extractAuthentication(map);
		AccessTokenMapper details = new AccessTokenMapper();
		if(map.get("id") != null)
			details.setId( (String) map.get("id"));
		if(map.get("username") != null)
			details.setId( (String) map.get("username"));
		if(map.get("email") != null)
			details.setId( (String) map.get("email"));
		if (auth.getAuthorities() != null && !auth.getAuthorities().isEmpty()) {
			List<String> authorities = new ArrayList<>();
			for (GrantedAuthority grantAuth : auth.getAuthorities()) {
				authorities.add(grantAuth.getAuthority());
			}
			details.setAuthorities(authorities);
		}
		auth.setDetails(details);
		return auth;
	}

	
	
	

}
