package com.example.springgradleresourceserver.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.springgradleresourceserver.helper.AccessTokenMapper;

@RestController
public class SampleController {
	
	@PreAuthorize("hasAnyRole('can_create_user')")
	@RequestMapping(value = "/permission", method = RequestMethod.GET)
	public String checkView() {
		AccessTokenMapper accessTokenMapper = (AccessTokenMapper) 
				((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails();
		
		System.out.println("accessToken -------- : " + accessTokenMapper.getUsername());
		return "view permitted";
	}

}
