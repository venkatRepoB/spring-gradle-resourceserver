package com.example.springgradleresourceserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/*Enables Spring Security global method security similar to the <global-method-security>xml support. 
More advanced configurations may wish to extend GlobalMethodSecurityConfiguration and override the protected methods to provide
custom implementations. Note that EnableGlobalMethodSecurity still must be 
included on the class extending GlobalMethodSecurityConfiguration to determine the settings.*/

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
//		super.configure(http);
		http
			.authorizeRequests().anyRequest().authenticated()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
	}
	

}
